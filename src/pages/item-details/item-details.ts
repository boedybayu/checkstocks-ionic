import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';
import { ProductProvider } from '../../providers/product/product';


@Component({
  selector: 'page-item-details',
  templateUrl: 'item-details.html'
})
export class ItemDetailsPage {

    selectedProduct: any;
    supplierProductData: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public productProvider: ProductProvider
    ) {
        // If we navigated to this page, we will have an product available as a nav param

        this.selectedProduct = navParams.get('product');

        this.getSupplierProductData(this.selectedProduct);
    }

    public getSupplierProductData(product: any) : void {

        var self = this;

        self.productProvider.getSupplierProductData(product).subscribe((response) => {
                self.supplierProductData = response.data;
            }, (err) => {
			    /*console.log(err);*/
            });
    }

    public generateArray(obj) : any {
        return Object.keys(obj).map((key)=>{ return {key:key, value:obj[key]}});
    }

}
