import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ListPage } from '../list/list';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

    public loading: boolean;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public authProvider: AuthProvider
    ) {
        this.loading = false;
    }

    onSubmit(form: any) {

        this.login(form.username, form.password);
    }

    login(username: string, password: string) {

        this.loading = true;

        this.authProvider.login(username, password)
            .subscribe(
                data => {
                    this.navCtrl.push(ListPage);
                },
                error => {
                    this.loading = false;
                });
    }

    ionViewDidLoad() {
	    //console.log('ionViewDidLoad LoginPage');
    }

}
