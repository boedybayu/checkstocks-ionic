import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { ItemDetailsPage } from '../item-details/item-details';
import { ProductProvider } from '../../providers/product/product';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

    public products: Object[];
    //public observedProducts: Object[];
    public observedProducts: any;
    public searchQuery: string = '';

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public productProvider: ProductProvider
    ) {
        // load products for current user
        this.initProducts();
    }

    /**
     * get products to be displayed
     */
    public initProducts(): void {

        var self = this;

        self.productProvider.getProducts().subscribe((response) => {
                self.observedProducts = response.data;
                self.products = response.data;
                self.products.sort(this.dynamicSort('code'));
            }, (err) => {
                //console.log(err);
            });

    }

    public productTapped(event, product) {

        this.navCtrl.push(ItemDetailsPage, {
            product: product
        });

    }

    public getItems(ev: any) {

        // set val to the value of the searchbar
        const val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {

            this.products = this.observedProducts.filter((product: any) => {
                var searchCode = false;
                var searchProductCode = false;

                if (product.code !== undefined && product.code != null) {
                    searchCode = product.code.toLowerCase().indexOf(val.toLowerCase()) > -1;
                }

                if (product.supplier_code !== undefined && product.supplier_code != null) {
                    searchProductCode = product.supplier_code.toLowerCase().indexOf(val.toLowerCase()) > -1;
                }

                return (
                    searchCode || searchProductCode
                );
            })

        } else {
            this.products = this.observedProducts;
        }

        this.products.sort(this.dynamicSort('code'));
    }

    public dynamicSort(property) {
        var sortOrder = 1;

        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }

        return function (a,b) {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        }
    }

}
