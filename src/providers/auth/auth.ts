import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constants from '../../app/configs/constants';
//import { Storage } from '@ionic/storage';
//import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

    protected loginParams: any;

    constructor(
        public http: HttpClient
        //private storage: Storage
    ) {}

    /**
     * login
     * @param string username
     * @param string password
     */
    public login(username: string, password: string) {

        var headers = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('X-Requested-With', 'XMLHttpRequest');

        return this.http.post<any>(
            Constants.API_ENDPOINT + '/auth/login',
            {email: username, password: password},
            {headers: headers}
        )
            .map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.access_token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    //this.storage.set('currentUser', user);
                    this.setCurrentUser(user);
                }
                return user;
            });
    }

    public setCurrentUser(user: {}): void {
        localStorage.setItem('user', JSON.stringify(user));
    }

    public getCurrentUser(): any {
        return JSON.parse(localStorage.getItem('user'));
    }

    public logout(): void {
        // remove user from local storage to log user out
        //this.storage.remove('currentUser');
        localStorage.clear();
    }
}
