//import { Storage } from '@ionic/storage';
import * as Constants from '../../app/configs/constants';
import { AuthProvider } from '../auth/auth';
import { HttpClient, HttpHeaders/*, HttpParams*/ } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
//import { map } from 'rxjs/operators';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

/*
  Generated class for the ProductProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProductProvider {

    protected user: any;

    constructor(
        public http: HttpClient,
        //private storage: Storage,
        private authProvider: AuthProvider
    ) {
        //console.log('Hello ProductProvider Provider');
    }

    /**
     * Get products base on the auth user
     *
     * @return Observable
     */
    public getProducts() : Observable<any> {

        var user = this.authProvider.getCurrentUser();

        var token = user.access_token;

        var headers = new HttpHeaders()
            .set('Authorization', 'Bearer '+ token)
            .set('Content-Type', 'application/json')
            .set('X-Requested-With', 'XMLHttpRequest');

        return this.http.get(
            Constants.API_ENDPOINT+'/products',
            {headers: headers}
        );

        /*
        return this.storage.get('currentUser').then(data => {

            var headers = new HttpHeaders()
                .set('Authorization', 'Bearer '+ data.access_token)
                .set('Content-Type', 'application/json')
                .set('X-Requested-With', 'XMLHttpRequest');

            return new Promise(
                resolve => {
                    this.http.get(Constants.API_ENDPOINT+'/products', {headers: headers})
                        .subscribe(
                            data => {resolve(data)}, 
                            err => {console.log(err)}
                        );
                },
            );
        });
         */

    }

    public search(terms: Observable<string>) {
        return terms
            .debounceTime(400)
            .distinctUntilChanged()
            .switchMap(term => this.searchEntries(term));
    }

    protected searchEntries(term) {
        return this.http
            .get(Constants.API_ENDPOINT + '/products/search?term=' + term)
            .map((res: any) => res.json());
    }

    /**
     * Get supplier product data
     *
     * @return Observable
     */
    public getSupplierProductData(product: any) : Observable<any> {

        var user = this.authProvider.getCurrentUser();

        var token = user.access_token;

        var headers = new HttpHeaders()
            .set('Authorization', 'Bearer '+ token)
            .set('Content-Type', 'application/json')
            .set('X-Requested-With', 'XMLHttpRequest');

        return this.http.get(
            Constants.API_ENDPOINT+'/supplier-product-data/'+product.id,
            {headers: headers}
        );

    }

}
